import template from './stas.html';
import controller from './stas.controller';
//import './stas.data.json';
import './stas.css';
import './stas.firebase.js';




let stasComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller,
  controllerAs:'stas'
};

export default stasComponent;