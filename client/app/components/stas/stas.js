import angular from 'angular';
import uiRouter from 'angular-ui-router';
import stasComponent from './stas.component';
import angularfire from 'angularfire';
import angularmaterial from 'angular-material';
import angularAnimate from 'angular-animate';
import angularSanitize from 'angular-sanitize';
import angularAria from 'angular-aria';



let stasModule = angular.module('stas', [
  uiRouter,
   angularfire,
   angularmaterial,
    angularAnimate,
    angularSanitize,
    angularAria
])

.config(($stateProvider) => {
  "ngInject";
  $stateProvider
    .state('stas', {
      url: '/stas',
      component: 'stas'
  
    });
})

.component('stas', stasComponent)
  
.name;


export default stasModule;