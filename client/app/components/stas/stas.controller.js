/*
import data from './stas.data.json';
class StasController {
  constructor(vm) {"ngInject";
    let vm = this;
    vm.name = 'stas';
    vm.data = data;
    let store = this;
    store.data = data;
    vm.eEditable= -1; 
    vm.AddPerson = function(){
            if(vm.formPerson.$valid) {
                store.data.push({
                    "id" : vm.Person.id,
                    "email" : vm.Person.email,
                    "firstname" : vm.Person.firstname,
                    "lastname" : vm.Person.lastname
                });
                vm.Person.id = "";
                vm.Person.email = "";
                vm.Person.firstname = "";
                vm.Person.lastname = "";
            }
    };
  }
    
}
*/

import * as firebase from "firebase";

class StasController {
  constructor($firebaseArray) {"ngInject";
    let ref = firebase.database().ref();
    let vm = this;
    vm.name = 'stas';
    vm.data = $firebaseArray(ref);
    console.log(vm.data);
    vm.eEditable= -1; 
    
            vm.AddPerson = function(){
                    
                        vm.data.push({
                            "id" : vm.Person.id,
                            "email" : vm.Person.email,
                            "firstname" : vm.Person.firstname,
                            "lastname" : vm.Person.lastname
                        });
                        console.log(vm.data);
                        vm.Person.id = "";
                        vm.Person.email = "";
                        vm.Person.firstname = "";
                        vm.Person.lastname = "";
                    
            };
    
            vm.delete = function(x) {
                let idx=vm.data.indexOf(x);
                vm.data.splice(idx,1);
               
            };

    
            vm.remove = function() {
                let oldList = vm.data;
                vm.data = [];
                angular.forEach(oldList, function(x) {
                    if (!x.done) vm.data.push(x);
                });
            };
    
            vm.checkAll = function () {
                if (vm.selectedAll) {
                    vm.selectedAll = true;
                } else {
                    vm.selectedAll = false;
                }
                angular.forEach(vm.data, function (x) {
                    x.done = vm.selectedAll;
                });
        
            };

  }
}

export default StasController;





