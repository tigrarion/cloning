import angular from 'angular';
import Home from './home/home';
import About from './about/about';
import Crud from './crud/crud';
import Stas from './stas/stas';

const componentModule = angular.module('app.components', [
  Home,
  About,
  Crud,
  Stas
])

.name;

export default componentModule;
